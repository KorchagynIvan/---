#pragma once
#include <vector>
#include "Cell.h"
#include "Coord.h"
#include "Ancestor.h"
class Field
{
private:
	Ancestor ***amount;
	int size;
	std::vector <Coord> Curr_alive;
	std::vector <Coord> Next_alive;
	std::vector <Coord> Curr_Zombie;
	std::vector <Coord> Next_Zombie;
// ����� ����, ���� ������ ����� ������ �������� ��������� � ����������
// next_gen() ���������� ����� ��������� �� ������ Curr_alive � ���������� ��� � Next_alive  
// (�������� � ����� ���������� �������� �� ���������� �������, �� ������ � ��� �������������)
// �����, Next_alive ���������� � Curr_alive � ��������� 
public:

	Field(int sizef);
	void next_gen();
	void show();
	void test_neigh();

	~Field();
};

